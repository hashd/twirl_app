# TwirlApp

Random experiment with Twitter API, d3 and web sockets for designing a concept user interface.

## Setting up
The project relies on exTwitter to consume Twitter stream APIs so don't forget to add configuration in `config/config.exs`.

```
config :extwitter, :oauth, [
   consumer_key: "",
   consumer_secret: "",
   access_token: "",
   access_token_secret: ""
]
```
