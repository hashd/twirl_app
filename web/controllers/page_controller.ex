defmodule TwirlApp.PageController do
  use TwirlApp.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
